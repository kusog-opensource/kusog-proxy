#include <drogon/drogon.h>
#include <map>
#include <string>
#include <regex>
#include <mutex>
#include <iostream>

using namespace drogon;

struct TargetDetails {
    std::string targetHost;
    uint16_t targetPort;
};

struct RoutingOption {
    TargetDetails defaultTarget;
    std::map<std::string, TargetDetails> specificPaths;
    std::map<std::regex, TargetDetails> regexPaths;
};

class RequestForwarder {
    std::map<std::string, RoutingOption> routingTable;
    std::mutex tableMutex;
    WebSocketClientPtr webSocketClient;  // Assume you have a WebSocket client setup elsewhere

public:
    RequestForwarder() {
        // Setup the WebSocket client and connect to the WebSocket server
    }

    void handleRequest(const HttpRequestPtr& req,
                       std::function<void(const HttpResponsePtr&)>&& callback) {
        std::lock_guard<std::mutex> guard(tableMutex);
        const auto& host = req->getHeader("Host");
        const auto& path = req->getPath();

        auto it = routingTable.find(host);
        if (it != routingTable.end()) {
            const auto& routingOption = it->second;
            TargetDetails targetDetails = routingOption.defaultTarget;

            // Check for specific path matches
            auto pathIt = routingOption.specificPaths.find(path);
            if (pathIt != routingOption.specificPaths.end()) {
                targetDetails = pathIt->second;
            } else {
                // Check for regex path matches
                for (const auto& [regex, details] : routingOption.regexPaths) {
                    if (std::regex_match(path, regex)) {
                        targetDetails = details;
                        break;
                    }
                }
            }

            // Broadcast summary to WebSocket before forwarding the request
            // Summary object creation logic should be based on the request
            std::string summary = "Summary of request to " + targetDetails.targetHost + ":" + std::to_string(targetDetails.targetPort);
            // Send the summary to the relevant WebSocket clients
            webSocketClient->send(summary);

            // Forward the request
            auto newReq = HttpRequest::newHttpRequest();
            *newReq = *req;
            newReq->addHeader("Host", targetDetails.targetHost);

            auto client = HttpClient::newHttpClient(targetDetails.targetHost, targetDetails.targetPort);
            client->sendRequest(newReq, [callback](ReqResult result, const HttpResponsePtr& resp) {
                if (result == ReqResult::Ok) {
                    callback(resp);
                } else {
                    auto errResp = HttpResponse::newHttpResponse();
                    errResp->setStatusCode(k500InternalServerError);
                    callback(errResp);
                }
            });
        } else {
            auto resp = HttpResponse::newNotFoundResponse();
            callback(resp);
        }
    }
};

