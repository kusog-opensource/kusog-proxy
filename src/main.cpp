int main() {
    auto forwarder = std::make_shared<RequestForwarder>();

    // Load the initial routing configuration from YAML files
    forwarder->loadConfiguration("path/to/config/dir");

    // Setup Drogon to use the RequestForwarder for handling incoming requests
    auto app = drogon::app();
    app.registerHandler("/{path:.*}", [forwarder](const HttpRequestPtr& req,
                                                  std::function<void(const HttpResponsePtr&)>&& callback) {
        forwarder->handleRequest(req, std::move(callback));
    });

    app.run();
    return 0;
}
