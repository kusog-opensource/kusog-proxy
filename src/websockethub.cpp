#include <drogon/WebSocketController.h>
#include <unordered_map>
#include <set>

using namespace drogon;

class WebSocketHubController : public WebSocketController<WebSocketHubController> {
public:
    WS_PATH_LIST_BEGIN
        WS_PATH_ADD("/ws/hub");
    WS_PATH_LIST_END

    // Called when a new WebSocket connection is established
    void handleNewConnection(const HttpRequestPtr& req, const WebSocketConnectionPtr& conn) override {
        // Here you can extract a contextId or hub name from the request
        // and associate the connection with that contextId or hub
        std::string hubName = req->getParameter("hub");
        connections[hubName].insert(conn);

        // Store context or hub information in the connection's context to use later
        conn->setContext(hubName);
    }

    // Called when a WebSocket connection is closed
    void handleConnectionClosed(const WebSocketConnectionPtr& conn) override {
        std::string hubName = conn->getContext<std::string>();
        auto& hubConnections = connections[hubName];
        hubConnections.erase(conn);
        if (hubConnections.empty()) {
            connections.erase(hubName);
        }
    }

    // Example method to broadcast a message to all clients in a specific hub
    void broadcastToHub(const std::string& hubName, const std::string& message) {
        if (connections.find(hubName) != connections.end()) {
            for (const auto& conn : connections[hubName]) {
                conn->send(message);
            }
        }
    }

private:
    std::unordered_map<std::string, std::set<WebSocketConnectionPtr>> connections;
};
