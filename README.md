
# Kusog Proxy

## Overview
This project is a high-performance C++ web service gateway developed using the Drogon framework. It dynamically routes HTTP requests based on configurations specified in YAML files and supports real-time monitoring and traffic control through WebSocket connections.

## Features
- **Dynamic Routing:** Routes HTTP requests based on hostnames and paths defined in YAML configurations.
- **Real-Time Monitoring:** Uses WebSocket to stream summaries of each incoming request to connected clients, facilitating real-time monitoring of traffic through the gateway.
- **Traffic Control:** Decides whether to forward requests based on rules related to source IP addresses, enabling IP-based traffic filtering.
- **WebSocket "Hubs":** Clients can subscribe to specific hubs (contexts) to receive data relevant to them, allowing for targeted monitoring and control.

## Prerequisites
- C++17 compatible compiler (e.g., GCC or Clang)
- Drogon framework installed
- yaml-cpp library installed

## Installation and Setup
1. **Clone the repository:**
   ```
   git clone https://gitlab.com/kusog-opensource/kusog-proxy.git
   cd kusog-opensource
   ```
2. **Build the project:**
   ```
   make
   ```
3. **Place your YAML configuration files in the `config/` directory.**

4. **Run the application:**
   ```
   make run
   ```

## Configuration
Routing configurations are defined in YAML files within the `config/` directory. An example configuration looks like this:

```yaml
example.com:
  default:
    host: "127.0.0.1"
    port: 8080
  specificPaths:
    "/api":
      host: "127.0.0.1"
      port: 8081
  regexPaths:
    "^/user/\\d+$":
      host: "127.0.0.1"
      port: 8082
```

## Real-Time Monitoring and Traffic Control
The gateway provides a WebSocket endpoint for real-time monitoring of traffic. Clients connected to this WebSocket can receive summaries of all processed requests. This feature is especially useful for monitoring systems that need to track the activity and health of the network traffic passing through the gateway.

Additionally, the gateway incorporates traffic control mechanisms that can block or allow requests based on source IP address rules. These rules can be dynamically configured, enabling administrators to respond to security threats or traffic patterns in real-time.

## Dynamic Configuration Updates
The service supports dynamic updates to routing configurations through exposed methods in the `RequestForwarder` class, allowing administrators to adjust routing rules without restarting the service.

## Contributing
We welcome contributions to this project. Please follow the standard Git workflow for submitting your changes:

1. Fork the repository.
2. Create a new branch for your feature or fix.
3. Commit your changes.
4. Push to your branch.
5. Submit a pull request.

## License
Specify your license here.
