CXX := g++  # Or clang++ if you prefer
CXXFLAGS := -std=c++17 -O2 -I/usr/include  # Adjust include path as needed
LDFLAGS := -ldrogon -lpthread -lyaml-cpp  # Link with required libraries

# Define your source and executable file
SRC := main.cpp
EXEC := app

.PHONY: all build run clean

all: build

build: $(SRC)
	$(CXX) $(CXXFLAGS) -o $(EXEC) $(SRC) $(LDFLAGS)

run: build
	./$(EXEC)

clean:
	rm -f $(EXEC)
